
/*
import { ActivityIndicator, Button, PlatformColor, SafeAreaView, StyleSheet, Text, ToastAndroid, View } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { BannerAdSize, GAMBannerAd, InterstitialAd, TestIds,AdEventType, AppOpenAd, RewardedAd, RewardedAdEventType } from 'react-native-google-mobile-ads'



const App = () => {

  // const [videoAdLoaded,setVideoAdLoaded]=useState(false)

  const bannerRef=useRef()


  const interstitial = InterstitialAd.createForAdRequest(TestIds.INTERSTITIAL, {
    requestNonPersonalizedAdsOnly: true,
    keywords: ['fashion', 'clothing'],
    
  });

  const appOpenAd = AppOpenAd.createForAdRequest(TestIds.APP_OPEN, {
    requestNonPersonalizedAdsOnly: true,
    keywords: ['fashion', 'clothing'],
  });

  const rewarded = RewardedAd.createForAdRequest(TestIds.REWARDED, {
    requestNonPersonalizedAdsOnly: true,
    // keywords: ['fashion', 'clothing'],
  });

  useEffect( () => {
    interstitial.load();
    appOpenAd.load()
    rewarded.load()


    // console.log(bannerRef.current)
  

    // rewarded.addAdEventListener(RewardedAdEventType.LOADED,()=>{
    //   ToastAndroid.show("Video Add Loaded",ToastAndroid.LONG)
    //   setVideoAdLoaded(true)
    // })

  }, [])
  



  const showInterstitialAds=()=>{
    if (interstitial.loaded) {
      interstitial.show();
    } else {
      ToastAndroid.show("Interstitial Ads Not Loaded",ToastAndroid.LONG)
    }
    
  }

  const showAppOpenAds=()=>{
    if (appOpenAd.loaded) {
      appOpenAd.show();
    } else {
      ToastAndroid.show("App Open Ads Not Loaded",ToastAndroid.LONG)
    }
  }

  const showRewardedVideoAds=()=>{
    if (rewarded.loaded) {
      rewarded.show();
    } else {
      ToastAndroid.show("Rewarded Video Ads Not Loaded",ToastAndroid.LONG)
    }
  }

  return (
    <SafeAreaView style={{flex:1,backgroundColor:'#fff',justifyContent:'space-evenly',alignItems:'center'}}>

<GAMBannerAd
    onAdFailedToLoad={(error=>{
      console.log(error.message)
      // console.log(error.stack)
      // console.log(error.name)
    })}
    // unitId={__DEV__?TestIds.BANNER:'ca-app-pub-6440861289573508/3956016361'}
    unitId={TestIds.BANNER}
    sizes={[BannerAdSize.LARGE_BANNER]}
    // ref={bannerRef}
    requestOptions={{
      requestNonPersonalizedAdsOnly: true,
    }}
  />

      <Button title='show Interstitial Ads' onPress={showInterstitialAds}/>

      <Button title='show App Open Ads' onPress={showAppOpenAds}/>

      <Button   title='show Video Ads' onPress={showRewardedVideoAds}/>

    </SafeAreaView>
  )
}

export default App

const styles = StyleSheet.create({})

*/

import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {NavigationContainer} from '@react-navigation/native';
import {store} from './src/Store/AppStore';
import {Provider} from 'react-redux';
import {ProvideTheme} from './src/Constants/Theme/Theme';
import MainNavigation from './src/Navigations/MainNavigation';


const App = () => {

  return (
    <Provider store={store}>
      <ProvideTheme>
      <NavigationContainer>
        <MainNavigation />
        </NavigationContainer>
      </ProvideTheme>
    </Provider>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;