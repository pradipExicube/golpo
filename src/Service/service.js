import axios from "axios";
const Api = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/",
});

const getDataWithOutToken = (endpoint) => {
  console.log(endpoint);
  return new Promise((resolve, reject) => {
    Api.get(endpoint, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.error(error);
        reject(error);
      });
  });
};

const getDataWithToken = (endpoint, token) => {
  console.log(endpoint, token);
  return new Promise((resolve, reject) => {
    Api.get(endpoint, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "x-access-token": token,
      },
    })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.error(error);
        reject(error);
      });
  });
};

const postWithToken = (endpoint, data, token) => {
  console.log(endpoint, data, token);
  return new Promise((resolve, reject) => {
    Api.post(endpoint, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "x-access-token": token,
      },
    })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.error(error);
        reject(error);
      });
  });
};

const postWithOutToken = (endpoint, data) => {
  console.log(endpoint, data);
  return new Promise((resolve, reject) => {
    Api.post(endpoint, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.error(error);
        reject(error);
      });
  });
};

export {
  Api,
  getDataWithOutToken,
  getDataWithToken,
  postWithOutToken,
  postWithToken,
};
