//import liraries
import React, {useEffect} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import { useTheme } from '../../Constants/Theme/Theme';
import Styles from './Style';
import LottieView from "lottie-react-native";

import * as Animatable from 'react-native-animatable';


// create a component
const Initial = props => {
  const {colorTheme} = useTheme();
  const styles=Styles()
  useEffect(() => {
    setTimeout(() => {
      props.navigation.replace('Home');
    }, 1000);
  }, []);
  return (
    <View style={styles.container}>
      <Animatable.View animation="rubberBand" duration={4000} iterationCount={'infinite'}  style={{flexDirection:'row',justifyContent:'center',flex:1}}>
          <Animatable.Image  source={require('../../assets/images/initial.png')}   style={{ height: 120, width:120, alignSelf: 'center', objectFit: 'cover' }} />
      </Animatable.View>
        <View>
          {/* <LottieView
                source={require("../../assets/lottie/initial.json")}
                autoPlay
                loop
            /> */}
        </View>
    </View>
  );
};


//make this component available to the app
export default Initial;
