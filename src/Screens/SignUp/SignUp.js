//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {ScreenLayout} from '../../Components';
import {useTheme} from '../../Constants/Theme/Theme';
import Styles from './Style';

// create a component
const SignUp = props => {
  const {colorTheme} = useTheme();
  const styles = Styles();
  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="SignUp">
      <View
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          onPress={() => props.navigation.navigate('Home')}
          style={{color: colorTheme.fontColor}}>
          SignUp
        </Text>
      </View>
    </ScreenLayout>
  );
};

//make this component available to the app
export default SignUp;
