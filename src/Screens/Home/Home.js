//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Styles from './Style';

// create a component
const Home = () => {
  const styles=Styles()
  return (
    <View style={styles.container}>
      <Text>Home</Text>
    </View>
  );
};



//make this component available to the app
export default Home;
