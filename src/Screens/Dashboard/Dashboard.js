//import liraries
import React, {Component, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import { useTheme } from '../../Constants/Theme/Theme';
import { postList } from '../../Service/Apis';
import {
  decrement,
  increment,
  setCount,
} from '../../Store/Reducers/CommonReducer';
import Styles from './Style';

import database from '@react-native-firebase/database';

// create a component
const Dashboard = () => {
  const {colorTheme}=useTheme()
  const styles = Styles();
  const dispatch = useDispatch();
  const {count} = useSelector(state => state.common);
  useEffect(()=>{
    // Api Call
    postList().then(resp=>{
      // console.log(resp.data);
    })
    // console.log("working")

    callData()
  },[])

// refData


  const callData =async() =>{
    console.log("csdjkbcskdjbkj")
      database()
        .ref('golpo_data')
        .once('value')
        .then(snapshot => {
          
          if (snapshot.val()) {
            const dataArray = Object.entries(snapshot.val())
              .map(([key, value]) => {
                console.log("value : ", key)

                  const innerData = Object.entries(value?.golpo_chapters)
                  .map(([key1, value1]) => {
                    console.log("value : ", key)
                    return {id: key1, ...value1};
                  })
                  // console.log("======== innerData ============", innerData)
                  // value.golpo_chapters = innerData;

                // return {id: key, ...value};
                return {id: key, ...value};
              })
              // .filter(item);


              console.log("======== dataArray ============", dataArray)
            // setUsers(dataArray);
          }
        }).catch((error)=>{
          console.log("error")
        })

    // console.log("csjhvcsd")
    //     database()
    //     .ref('refData')
    //     .once('value')
    //     .then(snapshot => {
    //       console.log("112")
    //       if (snapshot.val()) {
    //         console.log("firebase data : ", snapshot.val())
    //         // const dataArray = Object.entries(snapshot.val())
    //         //   .map(([key, value]) => {
    //         //     return {id: key, ...value};
    //         //   })
    //         //   .filter(item => userData?.email !== item?.email);
    //         // setUsers(dataArray);
    //       }
    //     })
    //     .catch((error)=>{console.log("error ", error)})


    /*
  let msgData = {
      message: "csdkcbskdcbvksdcvdkscvksdcdsc",
    };
    // console.log(msgData);
    const newReference = database()
      .ref('/messages/')
      .push();
    msgData.id = newReference.key;
    newReference.set(msgData).then(() => {
      console.log("data updated")
      // let chatListupdate = {
      //   lastMsg: message,
      //   sendTime: msgData.sendTime,
      // };
      // database()
      //   .ref('/chatlist/' + receiverData?.roomId)
      //   .update(chatListupdate)
      //   .then(() => console.log('Data updated.'));
      // //   console.log("'/chatlist/' + userData?.id + '/' + data?.id", receiverData);

      // setMessage('');
      // setdisabled(false);
    });

    */



  }





  return (
    <View style={styles.container}>
      <View
        style={{
          width: '100%',
          height: 50,
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth:1,
            borderRadius:5,
            borderColor:colorTheme.fontColor,
          }}
          onPress={() => dispatch(decrement(1))}>
          <Text style={{color: colorTheme.fontColor}}>-</Text>
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            borderColor:colorTheme.fontColor,
            borderWidth:1,
            borderRadius:5
          }}>
          <Text style={{color: colorTheme.fontColor}}>{count}</Text>
        </View>
        <TouchableOpacity
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth:1,
            borderRadius:5,
            borderColor:colorTheme.fontColor,
          }}
          onPress={() => dispatch(increment(1))}>
          <Text style={{color: colorTheme.fontColor}}>+</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

//make this component available to the app
export default Dashboard;
