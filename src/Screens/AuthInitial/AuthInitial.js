//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet, Switch} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useTheme} from '../../Constants/Theme/Theme';
import {
  setIsDarkMode,
  setIsStackHeaderVisible,
} from '../../Store/Reducers/CommonReducer';
import Styles from './Style';

// create a component
const AuthInitial = props => {
  const {colorTheme} = useTheme();
  const dispatch = useDispatch();
  const styles = Styles();
  const {isDarkMode, isStackHeaderVisible} = useSelector(state => state.common);
  return (
    <View style={styles.container}>
      <Switch
        value={isDarkMode}
        onValueChange={() => dispatch(setIsDarkMode(!isDarkMode))}
      />
      <Text
        onPress={() => {
          props.navigation.navigate('SignIn'),
            dispatch(setIsStackHeaderVisible(true));
        }}
        style={{color: colorTheme.fontColor, marginVertical: 10}}>
        SignIn
      </Text>
      <Text
        onPress={() => {
          props.navigation.navigate('SignUp'),
            dispatch(setIsStackHeaderVisible(true));
        }}
        style={{color: colorTheme.fontColor}}>
        SignUp
      </Text>
    </View>
  );
};

//make this component available to the app
export default AuthInitial;
