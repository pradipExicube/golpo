import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {AuthInitial, Home, Initial, SignIn, SignUp} from '../Screens';
import DrawerNavigation from './DrawerNavigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TouchableOpacity} from 'react-native';
import {useTheme} from '../Constants/Theme/Theme';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

const Stack = createStackNavigator();

const MainNavigation = props => {
  const {colorTheme} = useTheme();
  const dispatch = useDispatch();
  const {isStackHeaderVisible} = useSelector(state => state.common);
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        headerStyle: {
          backgroundColor: colorTheme.backGroundColor,
        },
        headerShadowVisible: false,
      }}>
      <Stack.Screen name="Initial" component={Initial} />
      <Stack.Screen name="AuthInitial" component={AuthInitial} />
      <Stack.Screen name="Home" component={DrawerNavigation} />
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        // options={{headerTitle: ''}}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        // options={{headerTitle: ''}}
      />
    </Stack.Navigator>
  );
};
export default MainNavigation;
