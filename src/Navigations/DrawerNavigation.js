import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {View, Text} from 'react-native';
import {Dashboard, Initial, SignIn} from '../Screens';
import {useTheme} from '../Constants/Theme/Theme';
import {windowHeight, windowWidth} from '../Constants/window';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Drawer = createDrawerNavigator();
const CustomDrawerContent = props => {
  const {colorTheme} = useTheme();
  return (
    <DrawerContentScrollView
      showsVerticalScrollIndicator={false}
      {...props}
      style={{backgroundColor: colorTheme.backGroundColor}}>
      <View
        style={{width: '100%', height: windowHeight, flexDirection: 'column'}}>
        <View
          style={{
            width: '100%',
            height: 120,
            flexDirection: 'column',
            borderBottomWidth:.5,
            borderBottomColor:'grey'
          }}>
          <View
            style={{
              height: 90,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="account-circle" size={70} color={colorTheme.fontColor}/>
          </View>
          <View style={{height: 30, width: '100%',alignItems:'center'}}>
            <Text style={{color: colorTheme.fontColor}}>Santanu Samanta</Text>
          </View>
        </View>
        <View style={{width: '100%', flex: 1}}>
          <DrawerItem
            label="Home"
            labelStyle={{color: colorTheme.fontColor, fontSize: 18}}
            onPress={() => {
              props.navigation.navigate('Dashboard');
            }}
          />
          <DrawerItem
            label="Logout"
            labelStyle={{color: colorTheme.fontColor, fontSize: 18}}
            onPress={() => {
              props.navigation.navigate('Initial');
            }}
          />
        </View>
      </View>
    </DrawerContentScrollView>
  );
};

const DrawerNavigation = () => {
  const {colorTheme} = useTheme();
  return (
    <Drawer.Navigator
      screenOptions={{
        // headerShadowVisible:false,
        headerStyle: {
          backgroundColor: colorTheme.backGroundColor,
        },
        headerTintColor: colorTheme.fontColor,
        drawerStyle: {
          width: '70%',
        },
      }}
      
      drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="Dashboard" component={Dashboard} />
      <Drawer.Screen
        name="Initial"
        component={Initial}
        options={{headerShown: false}}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
