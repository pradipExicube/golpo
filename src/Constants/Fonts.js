const FontSize = {
  f8: 8,
  f9: 9,
  f10: 10,
  f11: 11,
  f12: 12,
  f13: 13,
  f14: 14,
  f15: 15,
  f16: 16,
  f17: 17,
  f18: 18,
  f19: 19,
  f20: 20,
  f21: 21,
  f22: 22,
  f23: 23,
  f24: 24,
  f25: 25,
};
const FontFamily = {
  RobotoBold: 'Roboto-Bold',
  RobotoRegular: 'Roboto-Regular',
  RobotoMedium: 'Roboto-Medium',
};

export {FontFamily, FontSize};
