// App used colors
const Colors = {
  red: 'red',
  black: '#000',
  white: '#fff',
  gray:'gray',
  darkGray:'darkgray',
  lightGray:'lightgray'
};

// Defined Theme. Do not edit theme. if you want to change color modify corresponding color in 'Colors' //

const darkTheme = {
  fontColor: Colors.white,
  backGroundColor: Colors.black,
  topBottomBackground:Colors.darkGray,

};

const lightTheme = {
  fontColor: Colors.black,
  backGroundColor: Colors.white,
  topBottomBackground:Colors.lightGray
};

// Defined Theme. Do not edit theme. if you want to change color modify corresponding color in 'Colors' //

export {darkTheme, lightTheme};
