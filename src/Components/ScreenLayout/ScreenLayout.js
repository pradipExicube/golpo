//import liraries
import React, {Component} from 'react';
import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {useTheme} from '../../Constants/Theme/Theme';
import {windowHeight} from '../../Constants/window';
import Header from '../Header/Header';
import Styles from './Style';

// create a component
const ScreenLayout = ({isHeaderShown,isShownHeaderLogo,headerTitle, children}) => {
  const {colorTheme} = useTheme();
  const styles = Styles();
  return (
    <SafeAreaView style={{backgroundColor: colorTheme.backGroundColor}}>
      <View style={styles.container}>
        {isHeaderShown ? (
          <View style={{height: 60, width: '100%', backgroundColor: 'red'}}>
            <Header isShownHeaderLogo={isShownHeaderLogo} headerTitle={headerTitle} />
          </View>
        ) : (
          <></>
        )}
        <View
          style={{
            height: isHeaderShown ? windowHeight - 100 : windowHeight,
            width: '100%',
            backgroundColor: colorTheme.backGroundColor,
          }}>
          {children}
        </View>
      </View>
    </SafeAreaView>
  );
};

//make this component available to the app
export default ScreenLayout;
