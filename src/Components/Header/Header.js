//import liraries
import {useNavigation} from '@react-navigation/native';
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {useTheme} from '../../Constants/Theme/Theme';
import Styles from './Style';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Icons, Images} from '../../Constants/ImageIconContant';
import {FontSize} from '../../Constants/Fonts';

// create a component
const Header = ({isShownHeaderLogo, headerTitle}) => {
  const navigation = useNavigation();
  const styles = Styles();
  const {colorTheme} = useTheme();
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          width: '100%',
          height: '100%',
        }}>
        <View
          style={{
            width: 50,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{padding: 5}}>
            <Icon name="arrow-back" size={20} color={colorTheme.fontColor} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {isShownHeaderLogo ? (
            <Image source={Icons.icon1} />
          ) : (
            <Text style={{color: colorTheme.fontColor, fontSize: FontSize.f18}}>
              {headerTitle}
            </Text>
          )}
        </View>
        <View
          style={{
            width: 50,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}></View>
      </View>
    </View>
  );
};

//make this component available to the app
export default Header;
