import { configureStore } from '@reduxjs/toolkit'
import { commonReducer } from './Reducers'

export const store = configureStore({
  reducer: {
    common:commonReducer
  },
})