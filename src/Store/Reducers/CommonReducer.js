import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  token: '',
  isDarkMode: false,
  count: 0,

  //Navigation state
  isStackHeaderVisible: false,
};

export const commonReducer = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setIsDarkMode: (state, action) => {
      state.isDarkMode = action.payload;
    },
    setToken: (state, action) => {
      state.token = action.payload;
    },
    resetToken: state => {
      state.token = '';
    },
    resetCommonStore: state => {
      state.token = '';
    },
    increment: (state, action) => {
      state.count = state.count + action.payload;
    },
    decrement: (state, action) => {
      state.count = state.count - action.payload;
    },

    //Navigation State
    setIsStackHeaderVisible: (state, action) => {
      state.isStackHeaderVisible = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setIsDarkMode,
  setToken,
  resetToken,
  resetCommonStore,
  increment,
  decrement,


  //Navigation State
  setIsStackHeaderVisible
} = commonReducer.actions;

export default commonReducer.reducer;
